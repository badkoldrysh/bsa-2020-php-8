<?php

declare(strict_types=1);

namespace App\Actions\Image;

use App\Entities\User;
use App\Jobs\ImageJob;
use App\Notifications\ImageProcessedNotification;
use App\Services\Contracts\ImageApiService;
use App\Actions\Contracts\Response;
use App\Values\Image;
use Illuminate\Support\Facades\Auth;

class ApplyFilterAction
{
    private ImageApiService $imageApiService;

    public function __construct(ImageApiService $imageApiService)
    {
        $this->imageApiService = $imageApiService;
    }

    public function execute(Image $image, string $filter): Response
    {
        $user = User::all()[0];
        ImageJob::dispatch(
            $image,
            $filter,
            $user
        )->onConnection("beanstalkd");

        return new ApplyFilterResponse($image);
    }
}
