<?php

namespace App\Notifications;

use App\Values\Image;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\BroadcastMessage;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Notification;

class ImageProcessingFailedNotification extends Notification implements ShouldBroadcast
{
    use Queueable;

    private $image;
    private $message;

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct(Image $image, string $message)
    {
        $this->image = $image;
        $this->message = $message;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param mixed $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['broadcast', 'mail'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param mixed $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        return (new MailMessage)
            ->line('Dear John Dou,')
            ->line('')
            ->line('The applying the filter <filter_name> was failed to the image:')
            ->line('')
            ->line('<a href="' . $this->image->getSrc(). '"><img src="' . $this->image->getSrc() . '" alt="Image"></a>')
            ->line('')
            ->line('Best regards,')
            ->line('Binary Studio Academy!');
    }

    public function toBroadcast($notifiable)
    {
        return (new BroadcastMessage(
            [
                "status" => "failed",
                "message" => $this->message,
                "image" => [
                    "id" => $this->image->getId(),
                    "src" => $this->image->getSrc()
                ]
            ]
        ));
    }

    /**
     * Get the array representation of the notification.
     *
     * @param mixed $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [

        ];
    }
}
