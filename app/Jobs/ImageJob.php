<?php

namespace App\Jobs;

use App\Entities\User;
use App\Notifications\ImageProcessedNotification;
use App\Notifications\ImageProcessingFailedNotification;
use App\Services\ImageApiService;
use App\Values\Image;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;

class ImageJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    private $image;
    private $filter;
    private $user;

    /**
     * Create a new job instance.
     *
     * @param Image $image
     * @param string $filter
     * @param User $user
     */
    public function __construct(Image $image, string $filter, User $user)
    {
        $this->image = $image;
        $this->filter = $filter;
        $this->user = $user;
    }

    /**
     * Execute the job.
     *
     * @param ImageApiService $imageApiService
     * @return void
     */
    public function handle(ImageApiService $imageApiService)
    {
        $result = $imageApiService->applyFilter($this->image->getSrc(), $this->filter);

        $this->user->notify(
            new ImageProcessedNotification(
                new Image(
                    $this->image->getId(),
                    $result
                )
            )
        );
    }

    public function failed(\Exception $exception)
    {
        $this->user->notify(
            new ImageProcessingFailedNotification(
                new Image(
                    $this->image->getId(),
                    $this->image->getSrc()
                ),
               $exception->getMessage()
            )
        );
    }
}
